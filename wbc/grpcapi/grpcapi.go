// Copyright (c) 2013-2016 The btcsuite developers
// The WBC developers. Copyright (c) 2017

package grpcapi

import (
	"context"
	"errors"
	"log"
	"net"
	_ "net/http/pprof"
	"time"

	"bitbucket.org/siegfriedvmblockchain/siegfried/wbc/grpcapi/pingapi"
	"bitbucket.org/siegfriedvmblockchain/siegfried/wbc/grpcapi/wbcnodeapi"

	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/reflection"
)

const (
	address     = "localhost:50053"
	defaultName = "wbcnode"
)

const (
	port = ":50052"
)

var ConAPI *grpc.ClientConn = nil
var ClientApi *wbcnodeapi.WbcnodeapiSrvClient = nil

func PingapiServerStart() (err error) {

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	} else {
		log.Printf("GRPC server  listen: %s", port)

	}

	s := grpc.NewServer()

	pingapi.RegisterPingApiSrvServer(s, &pingapi.ServerGRPC{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
	return err
}

func WbcnodeapiClientConnect() (c *grpc.ClientConn) {

	if (ConAPI != nil) && (ConAPI.GetState() == connectivity.Ready) {
		return ConAPI
	}
	ConAPI, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Second*2)
	res := ConAPI.WaitForStateChange(ctx, connectivity.Idle)
	if !res {
		log.Printf("conn.WaitForStateChange( %s) ", ConAPI.GetState().String())
	}

	ctx, _ = context.WithTimeout(context.Background(), time.Second*2)
	res = ConAPI.WaitForStateChange(ctx, connectivity.Connecting)
	if !res {
		log.Printf("conn.WaitForStateChange( %s) ", ConAPI.GetState().String())
	}
	ctx, _ = context.WithTimeout(context.Background(), time.Second*2)
	res = ConAPI.WaitForStateChange(ctx, connectivity.Ready)
	if !res {
		log.Printf("conn.WaitForStateChange( %s) ", ConAPI.GetState().String())
	}

	state := ConAPI.GetState()
	if state != connectivity.Ready {
		log.Printf("conn.State() = %s", ConAPI.GetState().String())
		return nil
	}

	log.Printf("GRPC connect : %s", address)
	return ConAPI

}

func WbcnodeapiClientSetUp() (c *wbcnodeapi.WbcnodeapiSrvClient) {

	if ConAPI == nil {
		ConAPI = WbcnodeapiClientConnect()
	}

	if (ConAPI == nil) || (ConAPI.GetState() != connectivity.Ready) {
		log.Printf("Not respond grpc  server")
		return nil
	}

	ClientApi := wbcnodeapi.NewWbcnodeapiSrvClient(ConAPI)
	log.Printf("GRPC connect : %s", address)
	return &ClientApi

}

func TestwbcnodeapiConnect() {

	time.Sleep(time.Second * 3)

	for {

		time.Sleep(time.Second * 2)
		cc := WbcnodeapiClientSetUp()
		if cc != nil {

			response2, err := (*cc).GetNodeStatus(context.Background(), &wbcnodeapi.GetNodeStatusRequest{SessionID: "number two"})
			if err != nil {
				log.Printf("Error when calling GetNodeStatus: %s", err)
			}
			log.Printf("Response from server: %s", response2.JSONNodeStatus)
		} else {
			err := errors.New("Not response from server")
			log.Printf("Error  %s", err)
		}

	}

}
