package pingapi

import (
	"golang.org/x/net/context"
)

// ServerGRPC represents the gRPC server
type ServerGRPC struct {
}

// ServerGRPC   ;

func (s *ServerGRPC) Ping(ctx context.Context, in *PingRequest) (*PingResponse, error) {
	var nodestatus = "I live "
	return &PingResponse{JSONNodeStatus: nodestatus}, nil
}

func (s *ServerGRPC) Update(ctx context.Context, in *UpdateRequest) (*UpdateResponse, error) {

	//example to do
	var ClientID = in.ClientID
	var HostID = in.HostID
	var ContainerID = in.ContainerID
	var result = ClientID + HostID + ContainerID
	result = "Ok!!"
	//examle to do
	return &UpdateResponse{JSONNodeStatus: result}, nil
}
